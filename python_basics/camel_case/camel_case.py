#if a letter is detected as upper case, adds a space to the new word
#function returns the original word with spaces added before an upper letter
def divider(word):
    new_word = ""
    for i, letter in enumerate(word):
        if i and letter.isupper():
            new_word += ' '
        new_word += letter

    return new_word


def main():
    word = "camelCaseIsAwesome"
    new_word = divider(word)
    if new_word == "camel Case Is Awesome":
        print("Easy task")
    else:
        print("oops, not good...")


if __name__ == '__main__':
    main()
