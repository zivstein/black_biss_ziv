"""
Black Biss - Basic Python

Write a function that replace any second word with the word "Corona".
"""


def word_pandemic(in_array):
    array = in_array.split()
    for index in range(len(array)):
        if index % 2 != 0:
            array[index] = "Corona"
    txt = " ".join(array)
    return txt



# small test to check it's work.
if __name__ == '__main__':
    base_word = "Koala Bears are the cutest"
    sick_word = "Koala Corona are Corona cutest"

    if word_pandemic(base_word) == sick_word:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")
