import sys
import re
import operator


def print_words(filename):
    word_count = count_words(filename)
    # creates a list of all the keys from the dictionary sorted alphabetically
    sorted_keys = sorted(word_count.keys(), key=lambda x: x.lower())
    sorted_word_count = {}
    # creates a new sorted dictionary: key=word value=count
    for key in sorted_keys:
        value = word_count[key]
        sorted_word_count[key] = value
    #prints the word counter
    for key in sorted_word_count:
        print(str(key) + ": " + str(sorted_word_count[key]))




def print_top(filename):
    word_count = count_words(filename)
    # creates a new sorted dictionary: key=word value=count
    sorted_word_count = sorted(word_count.items(), key=operator.itemgetter(1), reverse=True)
    # prints the word counter
    for i in range(20):
        tpl = sorted_word_count[i]
        print(str(tpl[0]) + ": " + str(tpl[1]))


#returns a sorted dictionary: key=word, value=count
def count_words(filename):
    file = open(filename, 'r')
    file_txt = file.read()
    #removes all non-alphabetical characters from the text
    file_txt = re.sub(r"[^a-zA-Z']", " ", file_txt).lower()
    file_txt = file_txt.replace("'", "")
    #creates a list of all the words in the text
    word_array = file_txt.split()
    word_count = {}
    #creates a dictionary: key=word, value=count
    for word in word_array:
        if word in word_count:
            word_count[word] = word_count[word] + 1
        else:
            word_count[word] = 1
    return word_count



def main():
    if len(sys.argv) != 3:
        print("usage: ./wordcount.py {--count | ---topcount} file")
        sys.exit(1)

    option = sys.argv[1]
    filename = sys.argv[2]
    if option == '--count':
        print_words(filename)
    elif option == '--topcount':
        print_top(filename)

    else:
        print("unknown option: " + option)
        sys.exit(1)


if __name__ == '__main__':
    main()
