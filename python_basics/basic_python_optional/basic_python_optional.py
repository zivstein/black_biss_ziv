

def task_one(arr1, arr2):
    arr_combined = list(zip(arr1, arr2))
    return arr_combined


def task_two(tpl_array):
    dictionary = dict(tpl_array)
    return dictionary


def task_three():
    #originally written without using a loop
    pass


def task_four(arr1, arr2):
    arr_combined = task_one(arr1, arr2)
    dictionary = task_two(arr_combined)
    return dictionary


def task_five(arr1_big, arr2_small):
    arr1_trimmed = arr1_big[len(arr2_small):]
    dictionary = task_four(arr1_trimmed, arr2_small)
    return dictionary

