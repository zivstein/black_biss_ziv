"""
Black Biss - Advance Python

Write a function that calculate how many month one should save money to buy new car.
"""


def home_economics(startPriceOld, startPriceNew, savingPerMonth, percentLossByMonth):
    savings = 0
    price_old = startPriceOld
    price_new = startPriceNew
    num_of_months = 1
    if savings + price_old >= price_new:
        total_money = savings + price_old
        total_money = total_money - price_new
        tpl = (0, round(total_money))
        return tpl
    while True:
        for i in range(0,2):
            if i == 1:
                percentLossByMonth += 0.5
            price_old = price_old * ((100 - percentLossByMonth) / 100)
            price_new = price_new * ((100 - percentLossByMonth) / 100)

            savings += savingPerMonth
            total_money = savings + price_old
            if total_money >= price_new:
                total_money = total_money - price_new
                tpl = (num_of_months, round(total_money))
                return tpl
            num_of_months += 1






# small test to check it's work.
if __name__ == '__main__':

    ret = home_economics(2000, 8000, 1000, 1.5)
    if ret[0] == 6 and ret[1] == 766:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")
