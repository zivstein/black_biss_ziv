"""
Black Biss - Advance Python

Write a function that calculate how far is our ejected pilot.
Run this program and insert the following lines (each end with Enter):

UP 5
DOWN 3
LEFT 3
RIGHT 2
0

and the result should be 2
"""
import math


def ejected():
    vertical = 0
    horizontal = 0
    while True:
        user_input = input("Please enter the appropriate command: ")
        if user_input == "0":
            break
        user_input_split = user_input.split()
        direction = user_input_split[0]
        value = int(user_input_split[1])
        if direction == "UP":
            vertical += value
        elif direction == "DOWN":
            vertical -= value
        elif direction == "RIGHT":
            horizontal += value
        elif direction == "LEFT":
            horizontal -= value
    distance = math.sqrt(horizontal ** 2 + vertical ** 2)
    return round(distance)

# small test to check it's work.
if __name__ == '__main__':
    print(ejected())
