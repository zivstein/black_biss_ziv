"""
Black Biss - Advance Python

Write a function that read one line from the user with many potentional passwords seperated by comma ","
and print out only the valid password - by the following rules:
    * contain letter from each of the characters-sets:
        lower-case english, upper-case english, decimal digit,
        special characters *&@#$%^
    * dosn't contain other symbols (ignore white-spaces)
    * length between 6 and 12

"""
import re


def validate_passwords():
    passwords = input('Please enter the passwords divided by "," :')
    passwords.replace(" ", "")
    password_list = passwords.split(",")
    valid_passwords = ""
    for password in password_list:
        valid = True
        if re.search('[a-z]', password) is None:
            valid = False
        if re.search('[0-9]', password) is None:
            valid = False
        if re.search('[A-Z]', password) is None:
            valid = False
        if re.search('[@#$%^&*]', password) is None:
            valid = False
        if len(password) < 6 or len(password) > 12:
            valid = False
        if valid is True:
            valid_passwords += str(password) + ","
    print(valid_passwords)



# small test to check it's work.
if __name__ == '__main__':
    validate_passwords()
