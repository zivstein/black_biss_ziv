/**
 * function gets a linked_list and adds a new node in the n-th position
 */

// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

// ------------------------------ functions -----------------------------
struct node {
   int data;
   int key;
   struct node *next;
};

struct node *head = NULL;
struct node *current = NULL;

//insert link at the n-th location
void insertLocation(int key, int data, int n) {
   //create a link
    struct node *link = (struct node*) malloc(sizeof(struct node));

    link->key = key;
    link->data = data;

    struct node* current = head;
    if(n!= 0)
    {
        //finds the node before the n-th location
        for(int i=0; i<n - 2; ++i)
        {
            //if it is last node
            if(current->next == NULL) {
                return NULL;
            } else {
                //go to next link
                current = current->next;
            }
        }
        struct node* old_next = current->next;
        current->next = link;
        link->next = old_next;
    }
    else
    {
        insertFirst(key, data);
    }}



//insert link at the first location
void insertFirst(int key, int data) {
   //create a link
   struct node *link = (struct node*) malloc(sizeof(struct node));

   link->key = key;
   link->data = data;

   //point it to old first node
   link->next = head;

   //point first to new first node
   head = link;
}

//find a link with given key
struct node* find(int key) {

   //start from the first link
   struct node* current = head;

   //if list is empty
   if(head == NULL) {
      return NULL;
   }

   //navigate through list
   while(current->key != key) {

      //if it is last node
      if(current->next == NULL) {
         return NULL;
      } else {
         //go to next link
         current = current->next;
      }
   }

   //if data found, return the current Link
   return current;
}

//display the list
void printList() {
   struct node *ptr = head;
   printf("\n[ ");

   //start from the beginning
   while(ptr != NULL) {
      printf("(%d,%d) ",ptr->key,ptr->data);
      ptr = ptr->next;
   }

   printf(" ]");
}

void main()
{
    insertFirst(1,10);
    insertFirst(2,20);
    insertFirst(3,30);
    insertFirst(4,1);
    insertFirst(5,40);
    insertFirst(6,56);
    printList();
    insertLocation(7,22, 3);
    printList();
}
